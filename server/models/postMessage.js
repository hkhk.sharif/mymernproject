import mongoose from 'mongoose';

//we use {} for objects and [] for arrays 
const postSchema = mongoose.Schema({
    title: String,
    message: String,
    creator: String,
    tags: [String],
    selectedField: String,
    likeCounter: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});
 
const postMessage = mongoose.model('postMessage' , postSchema);
export default postMessage;