import express from 'express';
import {getPosts , createPost} from '../controllers/posts.js';
//above in Node you absoloutly need to add .js after the posts unlilke the react

const router = express.Router();

router.get( '/' , getPosts);
router.post( '/' , createPost);

export default router;