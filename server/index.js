import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import postRoutes from './routes/posts.js';

//we have more work to do for this section => need to create a DB and connect to it and build the model 
const app = express();

//blow means every routs in postRoutes start with /post

app.use('/posts' , postRoutes);
app.use(bodyParser.json({limit: "30mb" , extended: "true"}));
app.use(bodyParser.urlencoded({limit: "30mb" , extended: "true"}));
app.use(cors());


//now you should use mongo db and host the DB in the cloud 
const CONNECTION_URL = 'mongodb+srv://HosseinTest:HosseinTest@cluster0.rglbk.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const PORT = 3000 || process.env.PORT;
//everything in {} is a object for example blow it has 2 paramether  
mongoose.connect(CONNECTION_URL , {useUnifiedTopology : true , useNewUrlParser : true})
    .then( () => app.listen( PORT , () => console.log('server is up and listning to: $ {PORT}')))
    .catch((error) => console.log(error.message));

mongoose.set('useFindAndModify' , false);
 
